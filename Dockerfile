FROM ubuntu:latest
LABEL authors="MrLutin"
MAINTAINER MrLutin Software, <support@mrlutin.dev>

RUN apt-get update
RUN apt-get install -y curl git liquidsoap ffmpeg
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN adduser --disabled-login container

USER container
ENV  USER=container HOME=/home/container
WORKDIR /home/container

COPY ./entrypoint.sh /entrypoint.sh
CMD ["/bin/bash", "/entrypoint.sh"]
